-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: reservasi
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.39-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'Nabila','admin@gmail.com','admin');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookreservasi`
--

DROP TABLE IF EXISTS `bookreservasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookreservasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_timereservasi` int(11) NOT NULL,
  `id_property` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `notelp` varchar(100) DEFAULT NULL,
  `tanggal` varchar(100) DEFAULT NULL,
  `status_pesanan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bookreservasi_fk` (`id_users`),
  KEY `bookreservasi_fk_1` (`id_timereservasi`),
  KEY `bookreservasi_fk_2` (`id_property`),
  CONSTRAINT `bookreservasi_fk` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bookreservasi_fk_1` FOREIGN KEY (`id_timereservasi`) REFERENCES `timereservasi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bookreservasi_fk_2` FOREIGN KEY (`id_property`) REFERENCES `property` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookreservasi`
--

LOCK TABLES `bookreservasi` WRITE;
/*!40000 ALTER TABLE `bookreservasi` DISABLE KEYS */;
INSERT INTO `bookreservasi` VALUES (7,1,4,1,'test','test@gmail.com','08221333','2019-12-18',5),(8,3,4,1,'testrakahikmah','rara@gmail.com','082212333123','2019-12-12',3),(9,4,7,1,'123123','test@gmail.com','081132333444','2019-12-14',3);
/*!40000 ALTER TABLE `bookreservasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `nama` text NOT NULL,
  `email` text NOT NULL,
  `phone` int(11) NOT NULL,
  `feedback` text NOT NULL,
  `saran` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedbackuser`
--

DROP TABLE IF EXISTS `feedbackuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedbackuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `point_pelayanan` int(11) NOT NULL,
  `point_detail` int(11) NOT NULL,
  `pointinformasi` int(11) NOT NULL,
  `pointrentanusia` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` date NOT NULL,
  `id_book` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `feedbackuser_fk` (`point_pelayanan`),
  KEY `feedbackuser_fk_1` (`point_detail`),
  KEY `feedbackuser_fk_2` (`pointinformasi`),
  KEY `feedbackuser_fk_3` (`pointrentanusia`),
  KEY `feedbackuser_fk_4` (`id_book`),
  CONSTRAINT `feedbackuser_fk` FOREIGN KEY (`point_pelayanan`) REFERENCES `pointfeedback` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `feedbackuser_fk_1` FOREIGN KEY (`point_detail`) REFERENCES `pointfeedback` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `feedbackuser_fk_2` FOREIGN KEY (`pointinformasi`) REFERENCES `pointinformasi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `feedbackuser_fk_3` FOREIGN KEY (`pointrentanusia`) REFERENCES `rentan_usia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `feedbackuser_fk_4` FOREIGN KEY (`id_book`) REFERENCES `bookreservasi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedbackuser`
--

LOCK TABLES `feedbackuser` WRITE;
/*!40000 ALTER TABLE `feedbackuser` DISABLE KEYS */;
INSERT INTO `feedbackuser` VALUES (1,3,4,2,4,'asadasdasd3333','2019-12-08',7),(2,5,5,4,4,'asdasdsad','2019-12-11',8),(3,5,5,4,4,'asdasd','2019-12-11',8);
/*!40000 ALTER TABLE `feedbackuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image1` text NOT NULL,
  `image2` text NOT NULL,
  `image3` text NOT NULL,
  `image4` text NOT NULL,
  `property_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (2,'hom1.jpg','greengar.jpg','hom1.jpg','home.png',3),(3,'8.2.jpg','rumah-minimalis-zaman-sekarang_d4oal1.jpg','images.jpg','rumah-sederhana8.jpg',4);
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inquiry`
--

DROP TABLE IF EXISTS `inquiry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inquiry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `subject` varchar(30) NOT NULL,
  `email` text NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inquiry`
--

LOCK TABLES `inquiry` WRITE;
/*!40000 ALTER TABLE `inquiry` DISABLE KEYS */;
/*!40000 ALTER TABLE `inquiry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pointfeedback`
--

DROP TABLE IF EXISTS `pointfeedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pointfeedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `point` int(11) NOT NULL,
  `keterangan_feedback` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pointfeedback`
--

LOCK TABLES `pointfeedback` WRITE;
/*!40000 ALTER TABLE `pointfeedback` DISABLE KEYS */;
INSERT INTO `pointfeedback` VALUES (1,5,'Memuaskan'),(2,4,'Baik'),(3,3,'Standar'),(4,2,'Buruk'),(5,1,'Sangat Buruk');
/*!40000 ALTER TABLE `pointfeedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pointinformasi`
--

DROP TABLE IF EXISTS `pointinformasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pointinformasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `point` int(11) NOT NULL,
  `keterangan_informasi` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pointinformasi`
--

LOCK TABLES `pointinformasi` WRITE;
/*!40000 ALTER TABLE `pointinformasi` DISABLE KEYS */;
INSERT INTO `pointinformasi` VALUES (1,4,'Sangat Membantu'),(2,3,'Membantu'),(3,2,'Informasi Kurang'),(4,1,'Sangat Kurang');
/*!40000 ALTER TABLE `pointinformasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property`
--

DROP TABLE IF EXISTS `property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `bedroom` int(11) NOT NULL,
  `hall` int(11) NOT NULL,
  `kitchen` int(11) NOT NULL,
  `bathroom` int(11) NOT NULL,
  `balcony` int(11) NOT NULL,
  `price` varchar(11) NOT NULL,
  `sqr_price` varchar(11) NOT NULL,
  `address` text NOT NULL,
  `video` text NOT NULL,
  `image` text NOT NULL,
  `description` varchar(300) NOT NULL,
  `location` text NOT NULL,
  `property_owner` varchar(20) NOT NULL,
  `property_type` varchar(50) NOT NULL,
  `lot_size` varchar(20) NOT NULL,
  `sold` varchar(12) NOT NULL,
  `land_area` varchar(20) NOT NULL,
  `date` datetime NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property`
--

LOCK TABLES `property` WRITE;
/*!40000 ALTER TABLE `property` DISABLE KEYS */;
INSERT INTO `property` VALUES (3,'Rumah Green',4,2,1,1,1,'300000000','200000000','green garden estate','https://www.youtube.com/watch?v=B7SkAq_94J8&list=RDqCgjpLEzBkg&index=12','caf.jpg','rumah cantik','kosong','Ali Zainal','75','125','No','2000','2019-10-20 15:23:07',1),(4,'Minimalis ',5,1,2,2,-5,'2000000000','1750000000','Blok e3/2','https://www.youtube.com/watch?v=pLMMbz-rfsQ','rumah-minimalis-zaman-sekarang_d4oal1.jpg','Rumah Nyaman dengan nuansa minimalis','nothing','Afika Latifa','75','20000','yes','400000','2019-10-28 00:08:17',1),(6,'Minimalis ',5,2,1,3,1,'500000000','45000000','Blok e4/20','-','klasik2.jpg','rumah nyaman dan luas','-','Namika Laila','75','2000','yes','400000','2019-11-01 13:20:26',1),(7,'Minimalis ',2,2,2,2,2,'3','-1','f','kosong','Struktur organisasi indofood cibitung.png','f','f','w','apakek','ww','yes','w','2019-11-04 14:52:42',1);
/*!40000 ALTER TABLE `property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rentan_usia`
--

DROP TABLE IF EXISTS `rentan_usia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rentan_usia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `point` int(11) NOT NULL,
  `keterangan_rentanusia` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rentan_usia`
--

LOCK TABLES `rentan_usia` WRITE;
/*!40000 ALTER TABLE `rentan_usia` DISABLE KEYS */;
INSERT INTO `rentan_usia` VALUES (1,4,'17-25 Tahun'),(2,4,'26-35 Tahun'),(3,4,'36-50 Tahun'),(4,4,'>50 Tahun');
/*!40000 ALTER TABLE `rentan_usia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timereservasi`
--

DROP TABLE IF EXISTS `timereservasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timereservasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `waktu_awal` time DEFAULT NULL,
  `waktu_akhir` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timereservasi`
--

LOCK TABLES `timereservasi` WRITE;
/*!40000 ALTER TABLE `timereservasi` DISABLE KEYS */;
INSERT INTO `timereservasi` VALUES (1,'08:00:00','10:00:00'),(2,'10:00:00','12:00:00'),(3,'13:00:00','15:00:00'),(4,'15:00:00','17:00:00');
/*!40000 ALTER TABLE `timereservasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id_users` int(4) NOT NULL AUTO_INCREMENT,
  `nik` char(16) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(10) NOT NULL,
  `password2` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_tlp` char(15) NOT NULL,
  `status` varchar(15) NOT NULL,
  `jk` varchar(15) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'101010103333','1234','nabila','nabila','nabila','nabila1o201i','12344444','lajang','perempuan','drvf');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'reservasi'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-29 16:37:09
