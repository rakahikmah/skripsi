<?php include('include/header.php');?>
<!-- main header end -->

<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Comment Section</h1>
            <ul class="breadcrumbs">
                <li><a href="index.php">Home</a></li>
                <li class="active">Comment Section</li>
            </ul>
        </div>
    </div>
</div>
<!DOCTYPE html>
<html>
<head>
<title>Feedback</title>
<link rel="stylesheet" type="text/css" href="stylef.css">
</head>
<body class="agileits_w3layouts">
    <h1 class="agile_head text-center">Feedback</h1>
    <div class="w3layouts_main wrap">
      <h6>Bantu Kami untuk memberikan Pelayanan terbaik untuk anda </h6>
        <form action="koneksifeedback.php" method="post" class="agile_form">
          <h6>Bagaimana Pelayanan kami terhadap anda?</h6>
             <ul class="agile_info_select">
                 <li><input type="radio" name="view" value="excellent" id="excellent" required> 
                      <label for="excellent">Memuaskan</label>
                      <div class="check w3"></div>
                 </li>
                 <li><input type="radio" name="view" value="good" id="good"> 
                      <label for="good"> Baik</label>
                      <div class="check w3ls"></div>
                 </li>
                 <li><input type="radio" name="view" value="neutral" id="neutral">
                     <label for="neutral">Biasa Saja</label>
                     <div class="check wthree"></div>
                 </li>
                 <li><input type="radio" name="view" value="poor" id="poor"> 
                      <label for="poor">Buruk</label>
                      <div class="check w3_agileits"></div>
                 </li>
             </ul>    
            <h5>Jika anda memiliki saran dan kritik lainnya, silahkan berikan tanggapan pada kolom ini :</h5>
            <textarea placeholder="Additional comments" class="w3l_summary" name="comments" required=""></textarea>
            <br><br>
            <input type="text" placeholder="Nama (optional)" name="name"  /><br><br></br>
            <input type="email" placeholder="Alamat Email (optional)" name="email"/><br><br>
            <input type="text" placeholder="Nomor Telepon (optional)" name="num"  /><br>
            <center><input type="submit" value="Kirim Feedback" class="agileinfo" /></center>
      </form>
    </div>
    <div class="agileits_copyright text-center">
            <p>Terimakasih sudah memberikan tanggapan ':)'</p>
    </div>
</body>
</html>

