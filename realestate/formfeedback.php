<?php include('include/header.php');


?>

<!-- main header start -->


<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <!-- <h1>Property Detail 1</h1> -->
            <ul class="breadcrumbs">
                <!-- <li><a href="index.html">Home</a></li> -->
                <!-- <li class="active">Property Detail 1</li> -->
            </ul>
        </div>
    </div>
</div>
<!-- Sub banner end -->

<!-- Properties details page start -->
<div class="properties-details-page content-area-15">
    <div class="container">
        <div class="row">
        
        
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-left: 30px;">
                
                <div class="card">
                    <div class="card-header">
                        <h4><center>Form pengisian feedback</center></h4>
                    </div>
                    <div class="card-body">
                        <form method="post" action="proses.php">
                            <input type="hidden" name="idtempatfeedback" value="<?=$_GET['id']?>">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><h5>Bagaimana pelayanan kami terhadap anda?</h5></label>
                                <div class="radio">
                                <label>
                                    <?php 
                                        $sql ="SELECT * FROM pointfeedback";
                                        $query =mysqli_query($con,$sql);
                                        while($data = mysqli_fetch_assoc($query)){
                                    ?>
                                        <h6><input type="radio" name="pointfeedback" id="optionsRadios1" value=<?=$data['point']?> <?php echo ($data['id'] == 1 ) ? "checked":""; ?> ><?=$data['keterangan_feedback']?></h6>
                                    <?php }?>
                                </label>
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1"><h5>Apakah informasi yang di berikan detail?</h5></label>
                                <div class="radio">
                                <label>
                                <?php 
                                        $sql ="SELECT * FROM pointfeedback";
                                        $query =mysqli_query($con,$sql);
                                        while($data = mysqli_fetch_assoc($query)){
                                    ?>
                                        <h6><input type="radio" name="pointfeedback2" id="optionsRadios1" value=<?=$data['point']?> <?php echo ($data['id'] == 1 ) ? "checked":""; ?> ><?=$data['keterangan_feedback']?></h6>
                                    <?php }?>
                                </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1"><h5>Apakah website ini membantu anda dalam mengetahui informasi tentang green garden estote</h5></label>
                                <div class="radio">
                                <label>
                                    <?php 
                                        $sql ="SELECT * FROM pointinformasi";
                                        $query =mysqli_query($con,$sql);
                                        while($data = mysqli_fetch_assoc($query)){
                                    ?>
                                        <h6><input type="radio" name="pointinformasi" id="optionsRadios1" value=<?=$data['point']?> <?php echo ($data['id'] == 1 ) ? "checked":""; ?> ><?=$data['keterangan_informasi']?></h6>
                                    <?php }?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1"><h5>Berapakah rentan usia andan</h5></label>
                                <div class="radio">
                                <label>
                                    <?php 
                                        $sql ="SELECT * FROM rentan_usia";
                                        $query =mysqli_query($con,$sql);
                                        while($data = mysqli_fetch_assoc($query)){
                                    ?>
                                        <h6><input type="radio" name="pointusia" id="optionsRadios1" value=<?=$data['point']?> <?php echo ($data['id'] == 1 ) ? "checked":""; ?> ><?=$data['keterangan_rentanusia']?></h6>
                                    <?php }?>
                                </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1"><h5>Jika anda memilik saran mengenai sistem ini. silahkan beri tanggapan</h5></label>
                                <div class="radio">
                                <label>
                                    <textarea class="form-control" name="komentar" rows="5" cols="60"></textarea>
                                </label>
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-primary pull-right" name="proses_feedback" >Simpan tanggapan anda</button>
                        </form>
                    
                    </div>
                </div>
            
                    
            </div>
        
            
                
        </div>
    </div>
</div>
<!-- Properties details page end -->

<!-- Footer start -->
<footer class="footer">
    <div class="container footer-inner">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item">
                    <h4>Contact Us</h4>

                    <ul class="contact-info">
                        <li>
                            Address: 20/F Green Road, Dhanmondi, Dhaka
                        </li>
                        <li>
                            Email: <a href="mailto:info@themevessel.com">info@themevessel.com</a>
                        </li>
                        <li>
                            Phone: <a href="tel:+0477-85x6-552">+XXXX XXXX XXX</a>
                        </li>
                        <li>
                            Fax: +XXXX XXXX XXX
                        </li>
                    </ul>

                    <ul class="social-list clearfix">
                        <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#" class="rss"><i class="fa fa-rss"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                <div class="footer-item">
                    <h4>
                        Useful Links
                    </h4>
                    <ul class="links">
                        <li>
                            <a href="#"><i class="fa fa-angle-right"></i>About us</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-angle-right"></i>Service</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-angle-right"></i>Properties Listing</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-angle-right"></i>Properties Grid</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-angle-right"></i>Contact Us</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-angle-right"></i>Blog</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-angle-right"></i>Property Details</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="recent-posts footer-item">
                    <h4>Recent Properties</h4>
                    <div class="media mb-4">
                        <a href="properties-details.php">
                            <img src="assets/img/sub-property/sub-property.jpg" alt="sub-property">
                        </a>
                        <div class="media-body align-self-center">
                            <h5>
                                <a href="properties-details.php">Beautiful Single Home</a>
                            </h5>
                            <p>February 27, 2018</p>
                            <p> <strong>$245,000</strong></p>
                        </div>
                    </div>
                    <div class="media mb-4">
                        <a href="properties-details.phpl">
                            <img src="assets/img/sub-property/sub-property-2.jpg" alt="sub-property-2">
                        </a>
                        <div class="media-body align-self-center">
                            <h5>
                                <a href="properties-details.php">Sweet Family Home</a>
                            </h5>
                            <p>February 27, 2018</p>
                            <p> <strong>$245,000</strong></p>
                        </div>
                    </div>
                    <div class="media">
                        <a href="properties-details.php">
                            <img src="assets/img/sub-property/sub-property-3.jpg" alt="sub-property-3">
                        </a>
                        <div class="media-body align-self-center">
                            <h5>
                                <a href="properties-details.phpl">Real Luxury Villa</a>
                            </h5>
                            <p>February 27, 2018</p>
                            <p> <strong>$245,000</strong></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <h4>Subscribe</h4>
                    <div class="Subscribe-box">
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                        <form action="#" method="GET">
                            <p>
                                <input type="text" class="form-contact" name="email" placeholder="Enter Address">
                            </p>
                            <p>
                                <button type="submit" name="submitNewsletter" class="btn btn-block btn-color">
                                    Subscribe
                                </button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                <p class="copy">&copy;  2018 <a href="http://themevessel.com/" target="_blank">Theme Vessel</a>. Trademarks and brands are the property of their respective owners.</p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->

<!-- External JS libraries -->
<script src="assets/js/jquery-2.2.0.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.selectBox.js"></script>
<script src="assets/js/rangeslider.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/jquery.filterizr.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/backstretch.js"></script>
<script src="assets/js/jquery.countdown.js"></script>
<script src="assets/js/jquery.scrollUp.js"></script>
<script src="assets/js/particles.min.js"></script>
<script src="assets/js/typed.min.js"></script>
<script src="assets/js/dropzone.js"></script>
<script src="assets/js/jquery.mb.YTPlayer.js"></script>
<script src="assets/js/leaflet.js"></script>
<script src="assets/js/leaflet-providers.js"></script>
<script src="assets/js/leaflet.markercluster.js"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/maps.js"></script>
<script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0N5pbJN10Y1oYFRd0MJ_v2g8W2QT74JE"></script>
<script src="assets/js/ie-emulation-modes-warning.js"></script>
<!-- Custom JS Script -->
<script  src="assets/js/app.js"></script>
</body>

<!-- Mirrored from storage.googleapis.com/themevessel-xero/properties-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 30 Dec 2018 08:43:44 GMT -->
</html>