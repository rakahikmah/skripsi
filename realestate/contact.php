<?php include('include/header.php');?>
<!-- main header end -->

<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Contact Us</h1>
            <ul class="breadcrumbs">
                <li><a href="index.php">Home</a></li>
                <li class="active">Contact Us</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub banner end -->

<!-- Contact 1 start -->
<div class="contact-1 content-area-7">
    <div class="container">
        <div class="main-title">
            <h1>Contact Us</h1>
            <p> dapatkan layanan terbaik untuk mendapatkan rumah impian anda</p>
        </div>

            </div>

            <div class=" offset-lg-1 col-lg-4 offset-md-0 col-md-5">
                <div class="contact-info">
                    <h3>Contact Info</h3>
                    <div class="media">
                        <i class="fa fa-map-marker"></i>
                        <div class="media-body">
                            <h5 class="mt-0">Office Address</h5>
                            <p>Perumahan Green Garden Estate No 1 Blok A</p>
                        </div>
                    </div>
                    <div class="media">
                        <i class="fa fa-phone"></i>
                        <div class="media-body">
                            <h5 class="mt-0">Phone Number</h5>
                            <p>Office<a href="tel:0477-0477-8556-552">: 0370-662400</a> </p>
                            <p>Mobile<a href="tel:+0477-85x6-552">: 085716161476 </a> </p>
                        </div>
                    </div>
                    <div class="media mrg-btn-0">
                        <i class="fa fa-envelope"></i>
                        <div class="media-body">
                            <h5 class="mt-0">Email Address</h5>
                            <p><a href="#">info@greengarden.com</a></p>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact 1 end -->
<!-- Google Maps Start -->
    <div class="akame-google-maps-area">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3945.6715730129317!2d116.09952981433487!3d-8.531230788798025!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dcdc11219a46079%3A0x2ea32603ea99c5f5!2sGREEN%20GARDEN%20ESTATE!5e0!3m2!1sen!2sus!4v1572231168261!5m2!1sen!2sus" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <!-- Google Maps End -->


<!-- Footer start -->
<?php include('include/footer.php');?>