<?php include 'include/config.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Laporan </title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/all-themes.css" rel="stylesheet" />
</head>
<body style="background-color: white;">
    <div class="container">
        <center><h2>Laporan Data Pemesanan Reservasi</h2></center>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Nilai Pelayanan</th>
                            <th>Detail Informasi</th>
                            <th>Keterangan Informasi</th>
                            <th>Usia</th>
                            <th>Saran</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $query ="select * from feedbackuser inner join bookreservasi on bookreservasi.id = feedbackuser.id_book";
                        $runquery = mysqli_query($con,$query);
                        $no = 1;
                        while($data = mysqli_fetch_assoc($runquery)){
                        ?>
                        <tr>
                            <td><?=$no++?></td>
                            <td><?=$data['nama']?> </td>
                            <td><?php 
                                $pelayanan = "select * from pointfeedback where point = $data[point_pelayanan]";
                                $value = mysqli_query($con,$pelayanan);
                                $valuenilai = mysqli_fetch_assoc($value);
                                echo $valuenilai['keterangan_feedback'];
                            ?></td>
                            <td><?php
                            $pelayanan = "select * from pointfeedback where point = $data[point_detail]";
                            $value = mysqli_query($con,$pelayanan);
                            $valuenilai = mysqli_fetch_assoc($value);
                            echo $valuenilai['keterangan_feedback'];
                            ?></td>
                            <td><?php
                            $pelayanan = "select * from pointinformasi where point = $data[pointinformasi]";
                            $value = mysqli_query($con,$pelayanan);
                            $valuenilai = mysqli_fetch_assoc($value);
                            echo $valuenilai['keterangan_informasi'];
                            ?></td>
                            <td><?php
                            $pelayanan = "select * from rentan_usia where point = $data[pointrentanusia]";
                            $value = mysqli_query($con,$pelayanan);
                            $valuenilai = mysqli_fetch_assoc($value);
                            echo $valuenilai['keterangan_rentanusia'];
                            ?></td>
                            <td><?=$data['keterangan']?></td>
                            <td><?=$data['tanggal']?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
<!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="plugins/raphael/raphael.min.js"></script>
    <script src="plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="plugins/flot-charts/jquery.flot.js"></script>
    <script src="plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
    <!-- Select Plugin Js -->
    <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <script>
        window.print()

    </script>
</html>