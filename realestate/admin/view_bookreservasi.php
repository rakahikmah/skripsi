<?php include('include/header.php');?>
      
      <script type="text/javascript">




function ubahproses(id)
  {
    if(confirm("apakah anda ingin mengubah menjadi proses?"))
    {
      window.location.href='prosess.php?id='+id+'&proses=ubahproses';
    }
  }

  function ubahtolak(id)
  {
    if(confirm("apakah anda ingin menolak book tersebut"))
    {
      window.location.href='prosess.php?id='+id+'&proses=ubahtolak';
    }
  }

  function ubahselesai(id)
  {
    if(confirm("apakah anda ingin mengubah menjadi selesai"))
    {
      window.location.href='prosess.php?id='+id+'&proses=ubahselesai';
    }
  }

</script>
<!-- Header -->
<?php include('include/sidebar.php');?>
  <!-- #END# Left Sidebar -->
  <section class="content">
              <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2 style="text-align: center;">
                            Data Book Reservasi
                            </h2>
                    
                        </div>
                        
                        <div class="body">
                        <ul class="nav nav-tabs">
                            <li role="presentation" class="active"><a href="#datareservasi" data-toggle="tab"> Data Reservasi</a></li>
                            <li role="presentation" ><a href="#datafeedback" data-toggle="tab"> Data Feedback</a></li>
                        </ul>
                                <?php if(isset($_GET['message'])):?>
                                    <?php if($_GET['message'] == 'ubahproses'):?>
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Informasi</strong> Anda telah mengubah status menjadi ubah prosess
                                    </div>
                                    <?php elseif($_GET['message'] == 'ubahtolak'):?>
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Informasi</strong> Anda telah mengubah status menjadi ubah tolak
                                    </div>
                                    <?php elseif($_GET['message'] == 'ubahselesai'):?>
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Informasi</strong> Anda telah mengubah status menjadi selesai reservasi
                                    </div>
                                    <?php endif;?>
                                <?php endif;?>
                                
                                <div class="box-body">
                                    <div class="tab-content">  
                                        <div class="tab-pane fade in active" id="datareservasi">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-hover js-basic-example dataTable" id="tablereservasi">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Nama</th>
                                                            <th>Email</th>
                                                            <th>No telp</th>
                                                            <th>Jam Waktu</th>
                                                            <th>Nama Properti</th>
                                                            <th>Tanggal</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                        $query ="select bookreservasi.id,bookreservasi.email,bookreservasi.nama,bookreservasi.notelp,timereservasi.waktu_awal,timereservasi.waktu_akhir,property.property_type,property.title,bookreservasi.tanggal,bookreservasi.status_pesanan from bookreservasi inner join users on users.id_users = bookreservasi.id_users 
                                                        inner join property on property.id = bookreservasi.id_property 
                                                        inner join timereservasi on timereservasi.id = bookreservasi.id_timereservasi
                                                        ";
                                                        $runquery = mysqli_query($con,$query);
                                                        $no = 1;
                                                        while($data = mysqli_fetch_assoc($runquery)){
                                                        ?>
                                                        <tr>
                                                            <td><?=$no++?></td>
                                                            <td><?=$data['nama']?> </td>
                                                            <td><?=$data['email']?></td>
                                                            <td><?=$data['notelp']?></td>
                                                            <td><?=$data['waktu_awal']?> - <?=$data['waktu_akhir']?></td>
                                                            <td><?=$data['title']?></td>
                                                            <td><?=$data['tanggal']?></td>
                                                            <td>
                                                                <?php 
                                                                    $status = $data['status_pesanan'];
                                                                if($status == 1){ ?>
                                                                        <button class='btn btn-primary' onclick="ubahproses(<?=$data['id']?>)">Proses</button> |                                       
                                                                        <button class='btn btn-danger' onclick="ubahtolak(<?=$data['id']?>)">Tolak</button>
                                                                <?php  }else if($status == 2){ ?>
                                                                        <button class='btn btn-primary' onclick="ubahselesai(<?=$data['id']?>)">Ubah Selesai</button>
                                                                <?php }else if($status == 3){ ?>
                                                                        <button class='btn btn-primary' >User Lakukan Feedback</button>
                                                                <?php }else if($status == 5){ ?>
                                                                    <button class='btn btn-success' >User Sudah Melakukan Feedback</button>
                                                                <?php }else if($status == 4){ ?>
                                                                    <button class='btn btn-danger' >Reservasi Anda Ditolak</button>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="tab-pane fade in" id="datafeedback">
                                                    <table class="table table-bordered table-hover js-basic-example dataTable" id="tablefeedback">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Nama</th>
                                                            <th>Nilai Pelayanan</th>
                                                            <th>Detail Informasi</th>
                                                            <th>Keterangan Informasi</th>
                                                            <th>Usia</th>
                                                            <th>Saran</th>
                                                            <th>Tanggal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                        $query ="select * from feedbackuser inner join bookreservasi on bookreservasi.id = feedbackuser.id_book";
                                                        $runquery = mysqli_query($con,$query);
                                                        $no = 1;
                                                        while($data = mysqli_fetch_assoc($runquery)){
                                                        ?>
                                                        <tr>
                                                            <td><?=$no++?></td>
                                                            <td><?=$data['nama']?> </td>
                                                            <td><?php 
                                                                $pelayanan = "select * from pointfeedback where point = $data[point_pelayanan]";
                                                                $value = mysqli_query($con,$pelayanan);
                                                                $valuenilai = mysqli_fetch_assoc($value);
                                                                echo $valuenilai['keterangan_feedback'];
                                                            ?></td>
                                                            <td><?php
                                                            $pelayanan = "select * from pointfeedback where point = $data[point_detail]";
                                                            $value = mysqli_query($con,$pelayanan);
                                                            $valuenilai = mysqli_fetch_assoc($value);
                                                            echo $valuenilai['keterangan_feedback'];
                                                            ?></td>
                                                            <td><?php
                                                            $pelayanan = "select * from pointinformasi where point = $data[pointinformasi]";
                                                            $value = mysqli_query($con,$pelayanan);
                                                            $valuenilai = mysqli_fetch_assoc($value);
                                                            echo $valuenilai['keterangan_informasi'];
                                                            ?></td>
                                                            <td><?php
                                                            $pelayanan = "select * from rentan_usia where point = $data[pointrentanusia]";
                                                            $value = mysqli_query($con,$pelayanan);
                                                            $valuenilai = mysqli_fetch_assoc($value);
                                                            echo $valuenilai['keterangan_rentanusia'];
                                                            ?></td>
                                                            <td><?=$data['keterangan']?></td>
                                                            <td><?=$data['tanggal']?></td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                        </div>

                                    </div>
                                </div>
                        </div>
                    </div>
                 </div>
              </div>
</section>


<?php include'include/footer.php';?>


                          