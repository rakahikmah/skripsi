<?php include('include/header.php');?>
    <!-- Header -->
	
    <section>
       
	   <!-- Left Sidebar -->
<?php include('include/sidebar.php');?>
        <!-- #END# Left Sidebar -->

   
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">Jumlah Reservasi</div><?php 
                                $sql = "select count(id) as 'jumlah' from bookreservasi";
                                $runquery = mysqli_query($con,$sql);
                                $data = mysqli_fetch_assoc($runquery);
                                // $data['jumlah'];
                                ?>
                            <div class="number count-to" data-from="0" data-to="<?=$data['jumlah']?>" data-speed="15" data-fresh-interval="20">
                                <?=$data['jumlah']?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">home</i>
                        </div>
                        <div class="content">
                            <div class="text">Jumlah Property</div>
                            <?php 
                                $sql = "select count(id) as 'jumlah' from property";
                                $runquery = mysqli_query($con,$sql);
                                $data = mysqli_fetch_assoc($runquery);
                                // $data['jumlah'];
                                ?>
                            <div class="number count-to" data-from="0" data-to="<?=$data['jumlah']?>" data-speed="1000" data-fresh-interval="20"><?=$data['jumlah']?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">forum</i>
                        </div>
                        <div class="content">
                            <div class="text">Jumlah Feeback</div>
                            <?php 
                                $sql = "select count(id) as 'jumlah' from feedbackuser";
                                $runquery = mysqli_query($con,$sql);
                                $data = mysqli_fetch_assoc($runquery);
                                // $data['jumlah'];
                                ?>
                            <div class="number count-to" data-from="0" data-to="<?=$data['jumlah']?>" data-speed="1000" data-fresh-interval="20"><?=$data['jumlah']?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person</i>
                        </div>
                        <div class="content">
                        <?php 
                                $sql = "select count(id_users) as 'jumlah' from users";
                                $runquery = mysqli_query($con,$sql);
                                $data = mysqli_fetch_assoc($runquery);
                                // $data['jumlah'];
                                ?>
                            <div class="text">Jumlah Customer</div>
                            <div class="number count-to" data-from="0" data-to="<?=$data['jumlah']?>" data-speed="1000" data-fresh-interval="20"><?=$data['jumlah']?></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2 style="text-align: center;">
                            Data Home Dashboard
                            </h2>
                    
                        </div>
                        
                        <div class="body">
                        <ul class="nav nav-tabs">
                            <li role="presentation" class="active"><a href="#datareservasi" data-toggle="tab"> Data Reservasi</a></li>
                            <li role="presentation" ><a href="#datafeedback" data-toggle="tab"> Data Feedback</a></li>
                        </ul>
                                <?php if(isset($_GET['message'])):?>
                                    <?php if($_GET['message'] == 'ubahproses'):?>
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Informasi</strong> Anda telah mengubah status menjadi ubah prosess
                                    </div>
                                    <?php elseif($_GET['message'] == 'ubahtolak'):?>
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Informasi</strong> Anda telah mengubah status menjadi ubah tolak
                                    </div>
                                    <?php elseif($_GET['message'] == 'ubahselesai'):?>
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Informasi</strong> Anda telah mengubah status menjadi selesai reservasi
                                    </div>
                                    <?php endif;?>
                                <?php endif;?>
                                
                                <div class="box-body">
                                    <div class="tab-content">  
                                        <div class="tab-pane fade in active" id="datareservasi">
                                            <br>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <a href="cetakreportreservasi.php" target="_blank" class="btn btn-primary" name="cetak laporan"><i class="material-icons">print</i><span class="icon-name">Cetak Laporan Reservasi</span></a>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Nama</th>
                                                            <th>Email</th>
                                                            <th>No telp</th>
                                                            <th>Jam Waktu</th>
                                                            <th>Nama Properti</th>
                                                            <th>Tanggal</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                        $query ="select bookreservasi.id,bookreservasi.email,bookreservasi.nama,bookreservasi.notelp,timereservasi.waktu_awal,timereservasi.waktu_akhir,property.property_type,property.title,bookreservasi.tanggal,bookreservasi.status_pesanan from bookreservasi inner join users on users.id_users = bookreservasi.id_users 
                                                        inner join property on property.id = bookreservasi.id_property 
                                                        inner join timereservasi on timereservasi.id = bookreservasi.id_timereservasi
                                                        ";
                                                        $runquery = mysqli_query($con,$query);
                                                        $no = 1;
                                                        while($data = mysqli_fetch_assoc($runquery)){
                                                        ?>
                                                        <tr>
                                                            <td><?=$no++?></td>
                                                            <td><?=$data['nama']?> </td>
                                                            <td><?=$data['email']?></td>
                                                            <td><?=$data['notelp']?></td>
                                                            <td><?=$data['waktu_awal']?> - <?=$data['waktu_akhir']?></td>
                                                            <td><?=$data['title']?></td>
                                                            <td><?=$data['tanggal']?></td>
                                                            <td>
                                                                <?php 
                                                                    $status = $data['status_pesanan'];
                                                                if($status == 1){ ?>
                                                                    Menunggu Segera diproses
                                                                <?php  }else if($status == 2){ ?>
                                                                    sedang menuju resevarsi
                                                                <?php }else if($status == 4){?>
                                                                     reservasi ditolak
                                                                <?php }else if($status == 5){ ?>
                                                                    user sudah mengisi feedback
                                                                <?php }else if($status == 3){ ?>
                                                                    user melakukan feedback
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="tab-pane fade in" id="datafeedback">
                                            <br>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <a href="cetakreportfeedback.php" target="_blank" class="btn btn-primary" name="cetakreportfeedback"><i class="material-icons">print</i><span class="icon-name">Cetak Data Feedback</span></a>
                                                </div>
                                            </div>
                                            <br>
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Nama</th>
                                                            <th>Nilai Pelayanan</th>
                                                            <th>Detail Informasi</th>
                                                            <th>Keterangan Informasi</th>
                                                            <th>Usia</th>
                                                            <th>Saran</th>
                                                            <th>Tanggal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                        $query ="select * from feedbackuser inner join bookreservasi on bookreservasi.id = feedbackuser.id_book";
                                                        $runquery = mysqli_query($con,$query);
                                                        $no = 1;
                                                        while($data = mysqli_fetch_assoc($runquery)){
                                                        ?>
                                                        <tr>
                                                            <td><?=$no++?></td>
                                                            <td><?=$data['nama']?> </td>
                                                            <td><?php 
                                                                $pelayanan = "select * from pointfeedback where point = $data[point_pelayanan]";
                                                                $value = mysqli_query($con,$pelayanan);
                                                                $valuenilai = mysqli_fetch_assoc($value);
                                                                echo $valuenilai['keterangan_feedback'];
                                                            ?></td>
                                                            <td><?php
                                                            $pelayanan = "select * from pointfeedback where point = $data[point_detail]";
                                                            $value = mysqli_query($con,$pelayanan);
                                                            $valuenilai = mysqli_fetch_assoc($value);
                                                            echo $valuenilai['keterangan_feedback'];
                                                            ?></td>
                                                            <td><?php
                                                            $pelayanan = "select * from pointinformasi where point = $data[pointinformasi]";
                                                            $value = mysqli_query($con,$pelayanan);
                                                            $valuenilai = mysqli_fetch_assoc($value);
                                                            echo $valuenilai['keterangan_informasi'];
                                                            ?></td>
                                                            <td><?php
                                                            $pelayanan = "select * from rentan_usia where point = $data[pointrentanusia]";
                                                            $value = mysqli_query($con,$pelayanan);
                                                            $valuenilai = mysqli_fetch_assoc($value);
                                                            echo $valuenilai['keterangan_rentanusia'];
                                                            ?></td>
                                                            <td><?=$data['keterangan']?></td>
                                                            <td><?=$data['tanggal']?></td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                        </div>

                                    </div>
                                </div>
                        </div>
                    </div>
                 </div>
              </div>

        </div>
    </section>
<?php include'include/footer.php';?>
	
    